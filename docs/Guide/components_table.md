| component value   | description |
| --- | --- |
| **gas** | Gas particles |
| **halo** | Dark matter particles |
| **dm**   | Dark matter particles |
| **disk** | Old stars particles |
| **stars** | Stars particles |
| **bulge** | Bulge particles |
| **bndry** | bndry particles |