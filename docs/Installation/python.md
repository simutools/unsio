# Python wrapper installation
## Installing from PYPI

`unsio` can be easily installed using [pip](https://pypi.org/project/unsio/):

```console
pip install unsio -U
```

## Installing from source

The source code for `unsio` can be downloaded from [gitlab](https://gitlab.lam.fr/infrastructure/unsio)


### Clone the repository

```
git clone https://gitlab.lam.fr/infrastructure/unsio
```
### Install the requirements packages
  * hdf5
  * swig
  * cmake
  
### Install command
```
cd cloned_unsio_repository
pip install .
```