#!/bin/bash

rm -rf wheel dist

# build/repair wheel
for pyver in 8 9 10 11 12 13; do
    PYBIN=$(which "python3.${pyver}")
    if ! [ -z $PYBIN ]; then
        echo "=== $PYBIN ==="
        echo "=> build"
        $PYBIN -m build --wheel -C build-dir=build
        echo "=> repairwheel"
        unzip -j -o dist/unsio*cp3${pyver}*.whl *unsio.so -d /tmp/lib
        repairwheel -l /tmp/lib dist/unsio*cp3${pyver}*.whl -o wheel
    fi
done
