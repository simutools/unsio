#!/bin/bash

SRCDIR=${PWD}
# install & test wheel
for pyver in 8 9 10 11 12 13; do
    PYBIN=$(which "python3.${pyver}")
    if ! [ -z $PYBIN ]; then
        cd $SRCDIR
        echo "=== $PYBIN ==="
        echo "=> install"        
        $PYBIN -m pip uninstall -y unsio
        $PYBIN -m pip install wheel/unsio*cp3${pyver}*.whl
        echo "=> run unsio.test.ctestunsio"        
        cd /tmp
        $PYBIN -m unsio.test.ctestunsio --out ${SRCDIR}/test-cp3${pyver}.res
    fi
done
