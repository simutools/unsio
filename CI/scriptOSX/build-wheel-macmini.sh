#!/bin/bash

rm -rf wheel dist

export PATH=${PATH}:/Users/grunner/.local/bin

# Activate python environement
# Build wheel
for PYVER in 3.9 3.11 3.12 3.13; do
    # venv activate
    echo "Activate venv ~/venv/unsio.${PYVER}"
    source ~/venv/unsio.${PYVER}/bin/activate
    echo "=> build"
    python -m build
    echo "=> repairwheel"
    # install delocate-wheel : pipx install delocate
    VERSION=$(echo ${PYVER}|sed "s/\.//g")
    delocate-wheel dist/unsio*cp${VERSION}*.whl -w wheel
    # deactivate venv 
    deactivate
done
