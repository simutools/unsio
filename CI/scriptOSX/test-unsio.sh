#!/bin/bash

# Initialyze conda envoronement 
source ~/miniconda3/etc/profile.d/conda.sh
SRCDIR=${PWD}

# Install unsio wheel for every python
# Run test program
for PYVER in 38 39 310 311 312; do
    cd $SRCDIR    
    # conda activate
    echo "Activate py${PYVER}"
    conda activate py${PYVER}
    # uninstall previous packages
    pip uninstall -y unsio

    # install python-unsio
    pip install  wheel/*unsio*${PYVER}*-macosx*.whl
    # run test
    cd /tmp
    python -m unsio.test.ctestunsio --out test-${PYVER}.res
    # deactivate conda
    conda deactivate
done
