#!/bin/bash

rm -rf wheel dist

# Initialyze conda envoronement 
source ~/miniconda3/etc/profile.d/conda.sh

# Activate python environement
# Build wheel
for PYVER in 38 39 310 311 312; do
    # conda activate
    echo "Activate py${PYVER}"
    conda activate py${PYVER}
    echo "=> build"
    CC=clang CXX=clang++ python -m build --wheel
    echo "=> repairwheel"
    # unzip -j -o dist/unsio*cp${PYVER}*.whl *unsio.dylib -d /tmp/lib
    # repairwheel -l /tmp/lib dist/unsio*cp${PYVER}*.whl -o wheel
    delocate-wheel dist/unsio*cp${PYVER}*.whl -w wheel
    
    conda deactivate
done