#!/bin/bash


# Initialyze conda envoronement 

# Activate python environement
# Build wheel
for PYVER in  3.9  3.11 3.12 3.13; do
	echo "create ~/venv/unsio.${PYVER} venv for python${PYVER}"
        python${PYVER} -m venv ~/venv/unsio.${PYVER}
        echo "activate venv and install build module"
        source ~/venv/unsio.${PYVER}/bin/activate
        pip install build
        deactivate
done
