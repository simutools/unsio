#!/bin/bash


export PATH=${PATH}:/Users/grunner/.local/bin

# Install unsio wheel for every python
# Run test program
SRCDIR=${PWD}
for PYVER in 3.9 3.11 3.12 3.13; do
    cd $SRCDIR
    # venv activate
    echo "Activate venv ~/venv/unsio.${PYVER}"
    source ~/venv/unsio.${PYVER}/bin/activate
    # uninstall previous packages
    pip uninstall -y unsio
    # install unsio
    VERSION=$(echo ${PYVER}|sed "s/\.//g")
    pip install  wheel/*unsio*${VERSION}*-macosx*.whl
    # run test
    cd /tmp
    python -m unsio.test.ctestunsio --out test-${PYVER}.res
    # deactivate venv
    deactivate
done
