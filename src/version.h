// ============================================================================
// Copyright Jean-Charles LAMBERT - 2008-2025
// e-mail:   Jean-Charles.Lambert@lam.fr
// address:  Centre de donneeS Astrophysique de Marseille (CeSAM)
//           Laboratoire d'Astrophysique de Marseille
//           Pôle de l'Etoile, site de Château-Gombert
//           38, rue Frédéric Joliot-Curie
//           13388 Marseille cedex 13 France
//           CNRS U.M.R 7326
// ============================================================================
// See the complete license in LICENSE and/or "http://www.cecill.info".
// ============================================================================
/**
        @author Jean-Charles Lambert <Jean-Charles.Lambert@lam.fr>
 */
#ifndef UNS_VERSION_H
#define UNS_VERSION_H

#define UNSIO_MAJOR "1"
#define UNSIO_MINOR "3"
#define UNSIO_PATCH "3"
#define UNSIO_EXTRA ""

#endif // UNS_VERSION_H
